<?php

class ParticipationModel {
	private $studentID;

	public function __construct($studentID) {
		$this->studentID = $studentID;
	}

	public function hasNotMadeChoice() {
		
		return file_exists($this->getFileName()) == false;
	}

	public function isParticipating() {
		return file_get_contents($this->getFileName()) === "true";
	}

	public function optIn() {
		file_put_contents($this->getFileName(), "true");
	}

	public function optOut() {
		file_put_contents($this->getFileName(), "false");	
	}

	private function getFileName() {
		return "./data/" . md5($this->studentID);
	}
}