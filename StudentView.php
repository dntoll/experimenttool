<?php

class StudentView {

	public function getMyID() {

		if (isset($_GET["uid"])) {
			return $_GET["uid"];
		}
		throw new \Exception("no user id (uid) supplied");
	}

	public function noTasksAreSelected() {
		if (isset($_GET["task"])) {
			return false;
		}
		return true;
	}

	public function getSelectedTaskIndex() {
		if (isset($_GET["task"])) {


			$taskIndex =  $_GET["task"];

			return $taskIndex;
		}
		throw new \Exception("no task index (task) supplied");
	}



	public function showInvalidTask() {
		echo "<h2>Task invalid</h2>";
		echo "Something is wrong, maybe you have not finished the task before?";
		echo "<a href='?uid=". $this->getMyID() . "' > Back to task list </a>";
	}

	public function showOptOut() {
		require_once("participate.html");
		echo "<p>";
		echo "Vill du deltaga i experimentet?";
		echo "</p>";
		echo "<p>";
		echo "<a href='?uid=". $this->getMyID() . "&optin' > Ja, jag vill deltaga i experimentet</a> ";
		echo "<a href='?uid=". $this->getMyID() . "&optout' >Nej, jag gör denna laboration som en laboration </a>";
		echo "</p>";
	}

	public function doOptIn() {
		if (isset($_GET["optin"])) {
			return true;
		}
		return false;
	}

	public function doOptOut() {
		if (isset($_GET["optout"])) {
			return true;
		}
		return false;
	}

	

	


	public function showTask(Task $task) {
		echo "<h2>".$task->getTitle()."</h2>";
		echo "<pre>";
		echo $task->getDescription();
		echo "</pre>";
		
		echo "<p>";
		echo " <a href='". $task->getQuizURL() . "' target='_blank'>Påbörja uppgiften i ny flik.</a> ";
		echo "När du är färdig med uppgiften (bakgrunden lyser grönt) återvänd till ";
		echo "<a href='?uid=". $this->getMyID() . "' >listan av uppgifter.</a> <br/>";
		
		echo "</p>";
	}

	public function showExperimentInstructions() {
		require_once("experiment.html");
	}

	/**
	* @param array of Task $tasks
	*/
	public function showTasks(array $tasks) {


		echo "<h2>Mina laborationsuppgifter</h2>";
		
		foreach ($tasks as $key => $task) {

			if ($task->isCompleted() == false) {

				echo "<h3>" . $task->getTitle() ."</h3>";
				if ($task->canBeStarted()) {
					echo "<pre>";
					echo $task->getDescription();
					echo "</pre>";
					
				
					$start = " Återgå till uppgift ";
					if ($task->getNumTries() == 0) {
						echo " Ej startad uppgift!";
						$start = " Påbörja uppgift ";
					} else if ($task->isCompleted()) {
						echo " Avslutad uppgift!";
					} else {
						echo " Pågående uppgift!";
					}

					echo "<a href='". $task->getQuizURL() . "' >$start </a>";
				} else {
					echo "Uppgiften är ännu inte tillgänglig, avsluta ovanstående uppgift först.";
				}
			}
		}
		echo "<h2>Avslutade uppgifter<h2/>";

		foreach ($tasks as $key => $task) {

			if ($task->isCompleted() == true) {
				echo "<h3>" . $task->getTitle() ."</h3>";
				$start = " Återgå till uppgift ";
				echo " Avslutad uppgift!";
				echo "<a href='". $task->getQuizURL() . "' >$start </a>";
			}
		}



	}
}