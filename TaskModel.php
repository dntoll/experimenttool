<?php


require_once("Task.php");
require_once("settings.php");



class TaskModel {
	private $base = BASEURL;//"https://csquiz.lnu.se/src/";
	//private $base = "http://localhost/src";

	public function __construct() {
		$base1 = "https://raw.github.com/dntoll/quiz/master/code";

		

		$this->taskDescriptions = array();


		//load these from file?
		$tags = new TaskDescription("PHP Taggar", "$base1/tags.md", htmlentities("PHP uses use \"<?php\" and \"?".">\" to differentiate between php and output, correct the following code so that the php code is executed!
	remember that all empty characters are also sent to the output buffer so place \"<?php\" first in the file. 
	Task: Edit the code so that the output becomes 'Hello World'"), false);
		$arrays = new TaskDescription("PHP Arrays", "$base1/array.md", "In PHP we have arrays. Task: Find and correct the bug in the code so that the method returns the sum of the array content.", false);
		$code = new TaskDescription("PHP Code", "$base1/code.md", "Find the bug in the following code.", true);
		$code2 = new TaskDescription("PHP Code", "$base1/code2.md", "Find the bug in the following code.", true);
		$instance = new TaskDescription("PHP Instances of classes", "$base1/instance.md", "Task: Use the defined classes to show a list of products", false);
		

		$this->groupDuplications = array($tags,$arrays,$code,$instance);
		$this->groupNoDuplications = array($tags,$arrays,$code2, $instance);
	}

	public function getTasks($uid) {

		$students = $this->getStudents();

		if (isset($students[$uid]) == false) {
			throw new \Exception("the student id [$uid] does not exist");
		} 
		$groups = $this->getGroups();
		$ret = array();
		$lastWasCompleted = true;
		foreach ($groups[$students[$uid]] as $value) {
			$task = new Task($this->base, $uid, $lastWasCompleted, $value);

			$lastWasCompleted = $task->isCompleted();
			$ret[] = $task;
		}
		return $ret;
	}

	public function getStudents() {
		//load these from file?
		return array("Martin" => "duplication", 
					 "Daniel" => "duplication", 
					 "Tobias" => "duplication",
					 "John"   => "duplication",
					 "Mats"   => "duplication",
					 "Anne"   => "duplication",
					 "Martin" => "duplication",
					 "Johan"  => "duplication");
	}

	public function getGroups() {
		return array("duplication" => $this->groupDuplications, 
					 "control" => $this->groupNoDuplications);
	}
}