<?php


class TaskDescription {
	public $file;
	public $title;
	public $description;
	public $isExperiment;

	public function __construct($title, $file,$description,$isExperiment) {
		$this->title = $title;
		$this->file = $file;
		$this->description = $description;
		$this->isExperiment = $isExperiment;
	}
}

class Task {

	private $numTries = 0;
	private $time = 0;
	private $taskDescription;

	public function __construct($baseURL, $uid, $lastIsCompleted, TaskDescription $taskDescription) {
		$this->url = "?quizmd5=" . md5($taskDescription->file) . "&uid=$uid";
		$this->baseURL = $baseURL;
		$this->taskDescription = $taskDescription;

		if ($lastIsCompleted) {
			$this->getResult();
			$this->canBeStarted = true;
		} else {
			$this->canBeStarted = false;
		}
	}

	public function getQuizURL() {
		return $this->baseURL . $this->url;
	}

	public function getTitle() {
		return $this->taskDescription->title;
	}
	public function getDescription() {
		return $this->taskDescription->description;
	}
	

	public function getTaskURL() {
		return $this->taskDescription->file;
		
	}

	public function getNumTries() {
		return $this->numTries;
	}

	public function getTimeSpent() {
		return intval($this->time);
	}

	public function getResults() {
		return $this->taskResults;
	}

	public function canBeStarted() {
		return $this->canBeStarted;
	}

	public function isCompleted() {
		if ($this->getNumTries() == 0) {
			return false;
		}
		foreach ($this->getResults() as $key => $result) {
			if ($result != true) {
				return false;
			}
		}

		return true;
	}



	private function getResult() {

		$url = $this->baseURL ."/eval.php". $this->url;

		$context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
		$result = file_get_contents($url, false, $context);

		$removed = str_replace("]", "", $result);
		$parts = preg_split("/[\[]/", $removed);

		if (isset($parts[1]))
			$this->numTries = $parts[1];
		else {
			$this->numTries = 0;
			//var_dump($url);
			//var_dump($result);
		}
		$this->time = 0;
		$this->taskResults = array();

		if (count($parts) > 3) {
			$this->time = substr($parts[2], 6);
			
			for ($i = 3 ; $i < count($parts); $i++) {
				$this->taskResults[] = $parts[$i] === "true";
			}
		}
	}
}