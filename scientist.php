<?php

require_once("ScientistView.php");
require_once("TaskModel.php");

class ScientistController {
	public function __construct() {
		$this->model = new TaskModel();
		$this->view = new ScientistView($this->model);
		
	}


	public function viewResults() {

		$students = $this->model->getStudents();
		$groups = $this->model->getGroups();
		$this->view->showReport($students, $groups);
	}
}


$sc = new ScientistController();

$sc->viewResults();