<?php

class ScientistView {

	public function __construct(TaskModel $model) {
		$this->model = $model;
	}

	public function showReport(array $students, array $groups) {


		echo "<h1>Scientist View</h1>";
		echo "<table><thead><tr><th>group</th><th colspan='3'>tasks</th></tr></thead><tbody>";
		//$gid = 0;
		foreach ($groups as $groupname => $group) {
			$row = "<td>$groupname</td>";

			$timeSpent = array();
			foreach ($group as $task) {
				$row .= "<td><a href='$task->file'>$task->file</a></td>";
				$timeSpent[$task->file] = 0;
			}
			echo "<tr>$row</tr>";
			$numStudents = 0;
			
			foreach ($students as $student => $groupID) {
				if ($groupname  == $groupID) {
					$numStudents++;
					$row = "<td>$student</td>";

					$tasks = $this->model->getTasks($student);

					$studentTotalTime = 0;
					foreach ($tasks as $task) {

						$studentTotalTime += $task->getTimeSpent();
						if ($task->isCompleted()) {
							$row .= "<td>Done ".$task->getTimeSpent()."</td>";
							
							$timeSpent[$task->getTaskURL()] += $task->getTimeSpent();
						} else if ($task->getNumTries() > 0) {
							$row .= "<td>".$task->getTimeSpent()."</td>";
						} else {
							$row .= "<td>NA</td>";
						}

						
					}
					$row .= "<td>$studentTotalTime </td>";

					echo "<tr>$row</tr>";
				}
			}

			$row = "<td>$groupname Total:</td>";

			if ($numStudents > 0) {
				foreach ($group as $task) {
					$row .= "<td>". $timeSpent[$task->file]." / $numStudents = ".($timeSpent[$task->file]/$numStudents)."</td>";
					 
				}
			} else {
				$row .= "<td>". $timeSpent[$task->file]."</td>";
			}
			echo "<tr>$row</tr>";
			
			//$gid++;
		}

		echo "<tbody></table>";
		
	}
}