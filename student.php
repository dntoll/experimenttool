<?php

require_once("StudentView.php");
require_once("TaskModel.php");
require_once("ParticipationModel.php");

class StudentController {
	public function __construct() {
		$this->view = new StudentView();
		$this->model = new TaskModel();
		
	}


	public function viewMyTasks() {
		$studentId = $this->view->getMyID();

		$participationModel = new ParticipationModel($studentId);

		if ($participationModel->hasNotMadeChoice()) {
			if ($this->view->doOptIn()) {
				$participationModel->optIn();
			}
			if ($this->view->doOptOut()) {
				$participationModel->optOut();
			} 
		} 

		if ($participationModel->hasNotMadeChoice()) {
			$this->view->showOptOut();
		} else {
			if ($participationModel->isParticipating()) {
				$this->view->showExperimentInstructions();
			}

			$tasks = $this->model->getTasks($studentId);

			if ($this->view->noTasksAreSelected()) {
				$this->view->showTasks($tasks);
			} else {
				$taskIndex = $this->view->getSelectedTaskIndex();

				if (isset($tasks[$taskIndex]) && 
					$tasks[$taskIndex]->canBeStarted()) {
					$this->view->showTask($tasks[$taskIndex]);
				} else {
					$this->view->showInvalidTask();
				}
			}
		}

	}
}


$sc = new StudentController();


?>
<!DOCTYPE html>
<html>
    <head>
    <title>Instruktioner</title>
    <meta charset="utf-8">
    </head>
    <body>
<?php

	$sc->viewMyTasks();
?>

	</body>
</html>